#include <types.h>
#include <kern/unistd.h>
#include <clock.h>
#include <copyinout.h>
#include <syscall.h>
#include <lib.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <current.h>

/*
 * simple proc management system calls
 */
void sys__exit(int status)
{
    /* get address space of current process and destroy */
    struct addrspace *as = proc_getas();
    as_destroy(as);
    /* thread exits. proc data structure will be lost */

    /* First save the exit status and then  print a message */
    curthread->exit_status = status;
    kprintf("\n\nProgram exited with code: %d\n\n", status);

    thread_exit();

    panic("thread_exit returned (should not happen)\n");
}
