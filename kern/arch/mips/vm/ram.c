/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <lib.h>
#include <vm.h>
#include <mainbus.h>

#include "opt-freeram.h"
#include "opt-verbose.h"
#include "opt-mem_stats.h"
#include "opt-bitmap.h"

#if OPT_BITMAP
// Definition of bitmap macros
#define SetBit(A,k)     ( A[(k/32)] |= (1 << (k%32)) )
#define ClearBit(A,k)   ( A[(k/32)] &= ~(1 << (k%32)) )
#define TestBit(A,k)    ( A[((k)/32)] & (1 << ((k)%32)) )
#endif

vaddr_t firstfree; /* first free virtual address; set by start.S */

static paddr_t firstpaddr; /* address of first free physical page */
static paddr_t lastpaddr;  /* one past end of last free physical page */

#ifdef OPT_FREERAM
/* GLOBAL VARIABLES
 */
#define MAX_N_PAGES 128

#if OPT_BITMAP
unsigned int freeRamFrames[MAX_N_PAGES/32];
#else
static unsigned char freeRamFrames[MAX_N_PAGES];
#endif
static unsigned int allocSize[MAX_N_PAGES];
static int nPages;

/*
 * Called very early in system boot to figure out how much physical
 * RAM is available.
 */
void ram_bootstrap(void)
{
	size_t ramsize;
	int i, kernelPages;

	/* Get size of RAM. */
	ramsize = mainbus_ramsize();
	/*
	 * This is the same as the last physical address, as long as
	 * we have less than 512 megabytes of memory. If we had more,
	 * we wouldn't be able to access it all through kseg0 and
	 * everything would get a lot more complicated. This is not a
	 * case we are going to worry about.
	 */
	if (ramsize > 512 * 1024 * 1024)
	{
		ramsize = 512 * 1024 * 1024;
	}

	nPages = ramsize / PAGE_SIZE;

	lastpaddr = ramsize;

	/*
	 * Get first free virtual address from where start.S saved it.
	 * Convert to physical address.
	 */
	firstpaddr = firstfree - MIPS_KSEG0;
	kernelPages = firstpaddr/PAGE_SIZE + 1;
	#if OPT_BITMAP
	for(i=0; i < nPages; i++){
		if( i < kernelPages){
			ClearBit(freeRamFrames,i);
		} else {
			SetBit(freeRamFrames, i);
		}
		allocSize[i] = 0;
	}
	#else
	for(i=0; i < nPages; i++){
		if( i < kernelPages){
			freeRamFrames[i] = 0;
		} else {
			freeRamFrames[i] = 1;
		}
		allocSize[i] = 0;
	}
	#endif
	kprintf("%uk physical memory available\n",
			(lastpaddr - firstpaddr) / 1024);
}

/*
 * This function is for allocating physical memory prior to VM
 * initialization.
 *
 * The pages it hands back will not be reported to the VM system when
 * the VM system calls ram_getsize(). If it's desired to free up these
 * pages later on after bootup is complete, some mechanism for adding
 * them to the VM system's page management must be implemented.
 * Alternatively, one can do enough VM initialization early so that
 * this function is never needed.
 *
 * Note: while the error return value of 0 is a legal physical address,
 * it's not a legal *allocatable* physical address, because it's the
 * page with the exception handlers on it.
 *
 * This function should not be called once the VM system is initialized,
 * so it is not synchronized.
 */
paddr_t
ram_stealmem(unsigned long npages)
{
	int i, first, found, np = (long) npages;
	paddr_t paddr = 0;
	// Search for np consecutive free pages.
	// Strategies: first fit.
	#if OPT_BITMAP
		for(i=0, first = -1, found = -1; i < nPages && found == -1; i++){
		if (TestBit(freeRamFrames,i))
		{	
			if (i == 0 || !TestBit(freeRamFrames,i-1))
				first = i; /* set first free in an interval */
			if (i - first + 1 >= np)
			{
				found = first;
				break;
			}
		}
	}
	if (found >= 0)
	{	
		#if OPT_VERBOSE
		kprintf("Allocated %d frame: ", np);
		for (i = found; i < found + np; i++)
		{
			ClearBit(freeRamFrames, i);
			kprintf("%d ", i);
		}
		kprintf("\n");
		allocSize[found] = np;
		paddr = (paddr_t)found * PAGE_SIZE;
		#else
		for (i = found; i < found + np; i++)
		{
			ClearBit(freeRamFrames, i);
		}
		allocSize[found] = np;
		paddr = (paddr_t)found * PAGE_SIZE;
		#endif
	}
	#else
	for(i=0, first = -1, found = -1; i < nPages && found == -1; i++){
		if (freeRamFrames[i])
		{
			if (i == 0 || !freeRamFrames[i - 1])
				first = i; /* set first free in an interval */
			if (i - first + 1 >= np)
			{
				found = first;
				break;
			}
		}
	}
	if (found >= 0)
	{	
		#if OPT_VERBOSE
		kprintf("Allocated %d frame: ", np);
		for (i = found; i < found + np; i++)
		{
			freeRamFrames[i] = (unsigned char)0;
			kprintf("%d ", i);
		}
		kprintf("\n");
		allocSize[found] = np;
		addr = (paddr_t)found * PAGE_SIZE;
		#else
		for (i = found; i < found + np; i++)
		{
			freeRamFrames[i] = (unsigned char)0;
		}
		allocSize[found] = np;
		paddr = (paddr_t)found * PAGE_SIZE;
		#endif
	}
	#endif
	return paddr;
}

/*
 * This function is intended to be called by the VM system when it
 * initializes in order to find out what memory it has available to
 * manage. Physical memory begins at physical address 0 and ends with
 * the address returned by this function. We assume that physical
 * memory is contiguous. This is not universally true, but is true on
 * the MIPS platforms we intend to run on.
 *
 * lastpaddr is constant once set by ram_bootstrap(), so this function
 * need not be synchronized.
 *
 * It is recommended, however, that this function be used only to
 * initialize the VM system, after which the VM system should take
 * charge of knowing what memory exists.
 */
paddr_t
ram_getsize(void)
{
	return lastpaddr;
}

/*
 * This function is intended to be called by the VM system when it
 * initializes in order to find out what memory it has available to
 * manage.
 *
 * It can only be called once, and once called ram_stealmem() will
 * no longer work, as that would invalidate the result it returned
 * and lead to multiple things using the same memory.
 *
 * This function should not be called once the VM system is initialized,
 * so it is not synchronized.
 */
paddr_t
ram_getfirstfree(void)
{
	paddr_t ret;

	ret = firstpaddr;
	firstpaddr = lastpaddr = 0;
	return ret;
}

void ram_freepages(paddr_t addr, int npages){
	int index, i = 0;
	(void)i;
	index = addr/PAGE_SIZE;
#if OPT_VERBOSE
	kprintf("Freed pages: ");
#endif
#if OPT_BITMAP
	for(int i=index; i < index + npages; i++){
		SetBit(freeRamFrames, i);
		#if OPT_VERBOSE
		kprintf("%d ", i);
		#endif
	}
	#if OPT_VERBOSE
		kprintf("\n\n");
	#endif
	allocSize[index] = 0;
#else
	for(int i=index; i < index + npages; i++){
		freeRamFrames[i] = 1;
		#if OPT_VERBOSE
		kprintf("%d ", i);
		#endif
	}
	#if OPT_VERBOSE
	kprintf("\n\n");
	#endif
	allocSize[index] = 0;
#endif
	return;
}

void ram_freemem(vaddr_t addr){
	int np = allocSize[KVADDR_TO_PADDR(addr)/PAGE_SIZE];
	ram_freepages(KVADDR_TO_PADDR(addr), np);
};


#if OPT_MEM_STATS
void mem_stats(void){
	#if OPT_BITMAP
		int freeFrames=0, i = 0;
	int percentageOccupation = 0;
	kprintf("Memory statistics:\n\n");
	for (i = 0; i < nPages; i++)
	{
		freeFrames += TestBit(freeRamFrames, i) ? 1 : 0;
	}
	kprintf("Avaible %d of %d\n", freeFrames, nPages);
	percentageOccupation = 100 - (100 * freeFrames )/ nPages;
	kprintf("Actual occupation %d %%:\n", percentageOccupation);
	for(i = 0; i < 20; i++){
		if(i < percentageOccupation/5){
			kprintf("█");
		}
		else{
			kprintf("▒");
		}
	}
	kprintf("\n\n");
	#else
	int freeFrames=0, i = 0;
	int percentageOccupation = 0;
	kprintf("Memory statistics:\n\n");
	for (i = 0; i < nPages; i++)
	{
		freeFrames += freeRamFrames[i];
	}
	kprintf("Avaible %d of %d\n", freeFrames, nPages);
	percentageOccupation = 100 - (100 * freeFrames )/ nPages;
	kprintf("Actual occupation %d %%:\n", percentageOccupation);
	for(i = 0; i < 20; i++){
		if(i < percentageOccupation/5){
			kprintf("█");
		}
		else{
			kprintf("▒");
		}
	}
	kprintf("\n\n");
	#endif
}
#endif
#else
/*
 * Called very early in system boot to figure out how much physical
 * RAM is available.
 */
void ram_bootstrap(void)
{
	size_t ramsize;

	/* Get size of RAM. */
	ramsize = mainbus_ramsize();
	/*
	 * This is the same as the last physical address, as long as
	 * we have less than 512 megabytes of memory. If we had more,
	 * we wouldn't be able to access it all through kseg0 and
	 * everything would get a lot more complicated. This is not a
	 * case we are going to worry about.
	 */
	if (ramsize > 512 * 1024 * 1024)
	{
		ramsize = 512 * 1024 * 1024;
	}

	lastpaddr = ramsize;
	/*
	 * Get first free virtual address from where start.S saved it.
	 * Convert to physical address.
	 */
	firstpaddr = firstfree - MIPS_KSEG0;
	kprintf("%uk physical memory available\n",
			(lastpaddr - firstpaddr) / 1024);
}

/*
 * This function is for allocating physical memory prior to VM
 * initialization.
 *
 * The pages it hands back will not be reported to the VM system when
 * the VM system calls ram_getsize(). If it's desired to free up these
 * pages later on after bootup is complete, some mechanism for adding
 * them to the VM system's page management must be implemented.
 * Alternatively, one can do enough VM initialization early so that
 * this function is never needed.
 *
 * Note: while the error return value of 0 is a legal physical address,
 * it's not a legal *allocatable* physical address, because it's the
 * page with the exception handlers on it.
 *
 * This function should not be called once the VM system is initialized,
 * so it is not synchronized.
 */
paddr_t
ram_stealmem(unsigned long npages)
{
	paddr_t paddr;
	size_t size;
	size = npages * PAGE_SIZE;

	if (firstpaddr + size > lastpaddr)
	{
		return 0;
	}

	paddr = firstpaddr;
	firstpaddr += size;

	return paddr;
}

/*
 * This function is intended to be called by the VM system when it
 * initializes in order to find out what memory it has available to
 * manage. Physical memory begins at physical address 0 and ends with
 * the address returned by this function. We assume that physical
 * memory is contiguous. This is not universally true, but is true on
 * the MIPS platforms we intend to run on.
 *
 * lastpaddr is constant once set by ram_bootstrap(), so this function
 * need not be synchronized.
 *
 * It is recommended, however, that this function be used only to
 * initialize the VM system, after which the VM system should take
 * charge of knowing what memory exists.
 */
paddr_t
ram_getsize(void)
{
	return lastpaddr;
}

/*
 * This function is intended to be called by the VM system when it
 * initializes in order to find out what memory it has available to
 * manage.
 *
 * It can only be called once, and once called ram_stealmem() will
 * no longer work, as that would invalidate the result it returned
 * and lead to multiple things using the same memory.
 *
 * This function should not be called once the VM system is initialized,
 * so it is not synchronized.
 */
paddr_t
ram_getfirstfree(void)
{
	paddr_t ret;

	ret = firstpaddr;
	firstpaddr = lastpaddr = 0;
	return ret;
}
#endif