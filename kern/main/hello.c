#include "hello.h"
#include <types.h>
#include <lib.h>
#include "opt-hello.h"

#if OPT_HELLO
void hello(void)
{
    kprintf("Hello from OS/161!\n");
}
#endif